'''
Created on 11.06.2018

@author: markus
'''
from tkinter import *
import socket
import threading
import pickle

'''
Class ServerThread
will handle the server part of the network tic tac toe game
'''

class ClientThread(threading.Thread):
    def __init__(self, clientAddress, clientSocket):
        super().__init__()
        self.cSock = clientSocket
        self.cAddress = clientAddress
        print("New connection to ",clientAddress)
    def run(self):
        myList = []
        myList.append("one")
        while True:
            #serialization of the list
            data = pickle.dumps(myList)
            self.cSock.sendall(data)
            print("waiting for answer from {} ...".format(self.cAddress))
            data = self.cSock.recv(1024)
            #de-serialization
            myList = pickle.loads(data)
            inPut = input("Add something to the list:")
            myList.append(inPut)
            

#create and start the server socket
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(("127.0.0.1",50000))
print("server is running")
#main loop of the server - wait for client, start thread and keep waiting
while True:
    server.listen(1)
    clientSocket, clientAddress = server.accept()
    newThread = ClientThread(clientAddress, clientSocket) 
    newThread.start()