'''
Created on 11.06.2018

@author: markus
'''

from tkinter import *
import socket
import threading
import pickle

serverIP = "127.0.0.1"
port = 50000

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((serverIP, port))

while True:
    data = client.recv(1024)
    data = pickle.loads(data)
    print("From SEerver: ",data)
    inPut = input("add to the list:")
    data.append(inPut)
    data = pickle.dumps(data)
    client.sendall(data)
